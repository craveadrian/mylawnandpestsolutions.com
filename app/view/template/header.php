<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:800|Poppins:200,300,300i,400,500,700">
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="hdLeft col-1 fl">
					<a href="<?php echo URL ?>;"> <img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name"); ?> Logo"> </a>
				</div>
				<div class="hdRight col-2 fl">
					<p> <img src="public/images/sprite.png" alt="phone" class="icon-phone1"> <span><?php $this->info(["phone","tel"]); ?></span></p>
					<p> <?php $this->info(["email","mailto"]); ?> </p>
					<nav>
						<a href="#" id="pull"><strong>MENU</strong></a>
						<ul>
							<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">Home</a></li>
							<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services">Services</a></li>
							<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery">Gallery</a></li>
							<li <?php $this->helpers->isActiveMenu("testimonials"); ?>><a href="<?php echo URL ?>testimonials">Testimonials</a></li>
							<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact">Contact Us</a></li>
						</ul>
					</nav>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="row">
				<p>Helping Create Your <span>Private Outdoor Oasis</span></p>
					<div class="discount">
						<div class="circle">
							<p>10%<span>OFF</span>
							<p>DISCOUNT FOR</p>
						</div>
						<p>Military</p>
						<p>Fireman</p>
						<p>Police</p>
						<p>Teacher</p>
					</div>
			</div>
		</div>
	<?php endif; ?>
