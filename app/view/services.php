<div id="content">
  <div class="row">
    <h1>Services</h1>
    <div class="inner-services">
      <div id="service1" class="svc-section">
        <h2>Effective Fertilizer Application Promotes Optimal Results</h2>
        <p>Lawn and Pest Solutions in Kaufman, Texas, provides yard maintenance services by an experienced team. We take our work seriously, so we get in and get the job done. Why pay for other companies' standard eight to 10 applications when we can get it done better with only six applications? We do our job quickly so you can get back to enjoying your yard.</p>

        <h3>Lawn Maintenance</h3>
        <p>Keep your lawn beautiful year round. Our lawn maintenance package includes six treatments a year, including two pre-emergents and four fertilizations. All of these treatments include weed control. We also offer a grub preventative, chinch bug control, and fire ant control. The cost varies by square footage of lawn.</p>
      </div>
      <div id="service2" class="svc-section">
        <h2>Complete Residential Pest Control</h2>
        <p>Don't let pests ruin your peace. Our Bi-monthly treatments let you relax at home and in the yard. The service includes an initial treatment of the interior and exterior, including insect bait around the perimeter of your foundation so pests stay away even when we aren't there. The cost is based on the size of your home, ranging from $79 to $139</p>

        <h3>Specific Nuisance Pest Control</h3>
        <p>When you have an infestation or one particular pest invading your home, we offer targeted removal for an additional charge. Fleas, ticks, roaches, and rodents don't stand a chance of moving into your home when we're in town.</p>
      </div>
      <div id="service3" class="svc-section">
        <h2>Mosquito Control</h2>
        <p>Perhaps the most irritating and health concerning biting insect that flies under the radar is the mosquito. We offer monthly control from March through October. We spray a mist through your shrubs and tree canopies to protect you and your family.</p>
        <img src="public/images/content/inner-svc-img.png" alt="Mosquito Control">
      </div>
    </div>
  </div>
</div>
