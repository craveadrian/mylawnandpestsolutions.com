<div id="content">
	<div class="row">
		<div class="company">
			<h1>Lawn Services for Consistently <br> Beautiful Yards</h1>
			<p>Lawn and Pest Solutions is beautifying lawns, one home and business property at a time. With our regularly scheduled lawn services, your neighbors will be envious of your perfect and lush lawn. The professionals at Lawn and Pest Solutions can also eliminate unwanted pests in your home or business, as well as your yard, so you can enjoy your space without the annoyance and health risks of an infestation.</p>
		</div>
	</div>
</div>
<div id="services">
	<div class="row">
		<div class="svcLeft inb">
			<h1>Our services include</h1>
			<p>Residential and Commercial</p>
			<ul>
				<li> Pest Control </li>
				<li> Weed control. </li>
				<li> Fertilization </li>
				<li> Fire ant treatments </li>
				<li> Grub preventive. </li>
				<li> Turf fungicide </li>
				<li> Mosquito Treatments</li>
			</ul>
			<img src="public/images/content/img1.jpg" alt="dog">
		</div>
		<div class="svcRight inb">
			<img src="public/images/content/img2.jpg" alt="lawn">
		</div>
	</div>
</div>
<div id="quote">
	<div class="row">
		<p>Enjoy Monthly Payment Plans When You Purchase At Least $500 Of Lawn Care Or Pest Control Services Per Year</p>
		<p><?php $this->info(["phone","tel"]); ?></p>
	</div>
</div>
<div id="about">
	<div class="row">
		<section>
			<h2>ABOUT US</h2>
			<p>Lawn and Pest Solutions in Kaufman, Texas, provides residential and commercial lawn services and pest control. Our company has over 10 years of positive experience in the lawn care and pest industries.</p>
			<p>We have the knowledge to ensure every job is done well. We’ll regularly assess your lawn, so we can make recommendations and changes as needed. When working with us, you can rest assure your property is well taken care of.</p>
			<p>It’s our goal to provide quality service to your satisfaction. We want to earn your trust, so we offer free service calls. Our approach is really simple: When you need professional lawn care and pest control, we provide it. We’re licensed from the Texas Department of Agriculture, and we are insured.</p>
		</section>
	</div>
</div>
<div id="featured-services">
	<div class="row">
		<h1>Featured Services</h1>
		<div class="images">
			<dl>
				<dt>	<img src="public/images/content/fsvc1.jpg" alt="weed">	</dt>
				<dd>Weed Control</dd>
			</dl>
			<dl>
				<dt>	<img src="public/images/content/fsvc2.jpg" alt="fertilization">	</dt>
				<dd>Fertilization Services</dd>
			</dl>
			<dl>
				<dt>	<img src="public/images/content/fsvc3.jpg" alt="homepest">	</dt>
				<dd>Home Pest Control</dd>
			</dl>
			<dl>
				<dt>	<img src="public/images/content/fsvc4.jpg" alt="mosquito">	</dt>
				<dd>Mosquito Control</dd>
			</dl>
		</div>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<div class="reviews">
			<dl>
				<dt> <p>Thank you for your Professional, and timely service, most importantly, your knowledge and expertise you showed in solving my problem with ants. It was a pleasure working you’re your company during my time of <br>need. Thank you</p> </dt>
				<dd> <p> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> - John D. </p> </dd>
			</dl>
			<dl>
				<dt> <p>Lawn and Pest Solutions handled an ant problem at my home. They were prompt, courteous and professional. The products they used were pet safe yet effectively ended the ant infestation. I would highly recommend Lawn and Pest Solutions and would definitely hire them again.</p> </dt>
				<dd> <p> <span>&#9733; &#9733; &#9733; &#9733; &#9733;</span> - Daisy A. </p> </dd>
			</dl>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<h1>Contact Us</h1>
			<div class="row-1">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
			</div>
			<label><span class="ctc-hide">Message</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
			</label>	
			<div class="g-recaptcha"></div>
			<label>
				<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
			</label><br>
			<?php if( $this->siteInfo['policy_link'] ): ?>
			<label>
				<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
			</label>
			<?php endif ?>
			<button type="submit" class="ctcBtn btn" disabled>SUBMIT</button>
			<div class="fr">
				<a href="<?php echo URL ?>"><img src="public/images/common/mainLogo.png" alt="<?php $this->info("company_name") ?> Main Logo" class="cntLogo"></a>
			</div>
			<div class="clearfix"></div>
		</form>
		<div class="cntDetails">
			<div class="col-1 inb">
				<dl>
					<dt class="inb"><img src="public/images/sprite.png" alt="Phone Icon" class="icon-phone2"></dt>
					<dd class="inb"><p> <span>Phone</span> <?php $this->info(["phone","tel"]); ?></p></dd>
				</dl>
				<dl>
					<dt class="inb"><img src="public/images/sprite.png" alt="Phone Icon" class="icon-email	"></dt>
					<dd class="inb"><p> <span>Mailing Address</span> <?php $this->info("address"); ?></p></dd>
				</dl>
			</div>
			<div class="col-2 inb">
				<dl>
					<dt class="inb"><img src="public/images/sprite.png" alt="Phone Icon" class="icon-location"></dt>
					<dd class="inb"><p> <span>SERVICE AREA</span> <?php $this->info("address2"); ?></p></dd>
				</dl>
			</div>
		</div>
	</div>
</div>
